<?php

namespace Drupal\signageos\Drush\Commands;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\digital_signage_framework\PlatformPluginManager;
use Drupal\signageos\Plugin\DigitalSignagePlatform\SignageOs;
use Drush\Attributes\Command;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file for signageOS applets.
 */
class AppletCommands extends DrushCommands {

  /**
   * The signageos plugin.
   *
   * @var \Drupal\signageos\Plugin\DigitalSignagePlatform\SignageOs
   */
  protected SignageOs $plugin;

  /**
   * Drush commands constructor.
   *
   * @param \Drupal\digital_signage_framework\PlatformPluginManager $platform_plugin_manager
   *   The platform plugin manager.
   */
  public function __construct(PlatformPluginManager $platform_plugin_manager) {
    parent::__construct();
    try {
      $plugin = $platform_plugin_manager->createInstance('signageos');
      if ($plugin instanceof SignageOs) {
        $this->plugin = $plugin;
      }
    }
    catch (PluginException) {
      // @todo Log this exception.
    }
  }

  /**
   * Return an instance of these Drush commands.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\signageos\Drush\Commands\AppletCommands
   *   The instance of Drush commands.
   */
  public static function create(ContainerInterface $container): AppletCommands {
    return new AppletCommands(
      $container->get('plugin.manager.digital_signage_platform')
    );
  }

  /**
   * Push latest version of the applet to signageOS platform.
   */
  #[Command(name: 'signageos:applet:push', aliases: ['sosap'])]
  #[Usage(name: 'signageos:applet:push', description: 'Pushes the latest applet to the platform.')]
  public function pushApplet(): void {
    $this->plugin->pushApplet();
  }

}
