<?php

namespace Drupal\signageos\Drush\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\digital_signage_framework\DeviceInterface;
use Drupal\digital_signage_framework\Emergency;
use Drupal\digital_signage_framework\Entity\Device;
use Drupal\digital_signage_framework\PlatformPluginManager;
use Drupal\signageos\Plugin\Action\PowerAction;
use Drupal\signageos\Plugin\DigitalSignagePlatform\SignageOs;
use Drush\Attributes\Argument;
use Drush\Attributes\Command;
use Drush\Attributes\Option;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file for digital signage devices.
 */
class DeviceCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The platform plugin manager.
   *
   * @var \Drupal\digital_signage_framework\PlatformPluginManager
   */
  protected PlatformPluginManager $pluginManager;

  /**
   * The emergency services.
   *
   * @var \Drupal\digital_signage_framework\Emergency
   */
  protected Emergency $emergency;

  /**
   * Constructs the device commands.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\digital_signage_framework\PlatformPluginManager $plugin_manager
   *   The platform plugin manager.
   * @param \Drupal\digital_signage_framework\Emergency $emergency
   *   The emergency services.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, PlatformPluginManager $plugin_manager, Emergency $emergency) {
    parent::__construct();
    $this->entityTypeManager = $entityTypeManager;
    $this->pluginManager = $plugin_manager;
    $this->emergency = $emergency;
  }

  /**
   * Return an instance of these Drush commands.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\signageos\Drush\Commands\DeviceCommands
   *   The instance of Drush commands.
   */
  public static function create(ContainerInterface $container): DeviceCommands {
    return new DeviceCommands(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.digital_signage_platform'),
      $container->get('digital_signage_content_setting.emergency'),
    );
  }

  /**
   * Execute power action on signageOS device.
   */
  #[Command(name: 'signageos:device:poweraction', aliases: ['sosdpa'])]
  #[Argument(name: 'deviceId', description: 'The device ID.')]
  #[Option(name: 'action', description: 'The optional action to execute.')]
  #[Usage(name: 'signageos:device:poweraction', description: 'Executes power action on signagePS device.')]
  public function powerAction(int $deviceId, array $options = ['action' => NULL]): void {
    $device = $this->loadDevice($deviceId, 'signageos');
    $plugin = $device->getPlugin();
    if (!($plugin instanceof SignageOs)) {
      $this->io()->error('This is not a signageOS device.');
      return;
    }
    $action = $this->input->getOption('action');
    if ($action === NULL) {
      $action = $this->io()->choice($this->t('Power action:'), PowerAction::powerActions());
    }
    $plugin->sendPowerAction($device, $action);
  }

  /**
   * Helper function to load a device entity.
   *
   * @param int $deviceId
   *   The device ID.
   * @param string|null $bundle
   *   The optional bundle.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The device entity.
   */
  protected function loadDevice(int $deviceId, ?string $bundle = NULL): DeviceInterface {
    /** @var \Drupal\digital_signage_framework\DeviceInterface|null $device */
    $device = Device::load($deviceId);
    if ($device === NULL) {
      throw new \InvalidArgumentException('Incorrect device ID');
    }
    if ($bundle !== NULL && $bundle !== $device->bundle()) {
      throw new \InvalidArgumentException('Device is not the correct type: ' . $bundle);
    }
    return $device;
  }

}
