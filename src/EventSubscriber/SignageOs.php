<?php

namespace Drupal\signageos\EventSubscriber;

use Drupal\digital_signage_framework\DigitalSignageFrameworkEvents;
use Drupal\digital_signage_framework\Event\Libraries;
use Drupal\digital_signage_framework\Event\Rendered;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for libraries and rendering.
 */
class SignageOs implements EventSubscriberInterface {

  /**
   * Gets dispatched when libraries are collected.
   *
   * @param \Drupal\digital_signage_framework\Event\Libraries $event
   *   The libraries event.
   */
  public function onLibraries(Libraries $event): void {
    $event->addLibrary('signageos/general');
  }

  /**
   * Gets dispatched when digital signage content gets rendered.
   *
   * @param \Drupal\digital_signage_framework\Event\Rendered $event
   *   The render event.
   */
  public function onRendered(Rendered $event): void {
    if ($event->getDevice()->bundle() === 'signageos') {
      $event->getResponse()->headers->set('Access-Control-Allow-Origin', 'https://hugstatic.blob.core.windows.net');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DigitalSignageFrameworkEvents::LIBRARIES => ['onLibraries'],
      DigitalSignageFrameworkEvents::RENDERED => ['onRendered'],
    ];
  }

}
