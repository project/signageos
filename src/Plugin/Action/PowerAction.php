<?php

namespace Drupal\signageos\Plugin\Action;

use Drupal\digital_signage_framework\Plugin\Action\Base;

/**
 * Execute power actions on devices.
 *
 * @Action(
 *   id = "signageos_power_action",
 *   label = @Translation("signageOS device Power Action"),
 *   type = "digital_signage_device",
 *   confirm_form_route_name = "signageos.poweraction"
 * )
 */
class PowerAction extends Base {

  /**
   * Gets the list of supported power actions.
   *
   * @return array
   *   The list of supported power actions.
   */
  public static function powerActions(): array {
    return [
      'APP_RESTART' => t('Restart'),
      'SYSTEM_REBOOT' => t('Reboot'),
      'APPLET_RELOAD' => t('Reload'),
      'APPLET_REFRESH' => t('Refresh'),
      'DISPLAY_POWER_ON' => t('Display power on'),
      'DISPLAY_POWER_OFF' => t('Display power off'),
      'APPLET_DISABLE' => t('Applet disable'),
      'APPLET_ENABLE' => t('Applet enable'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function permission(): string {
    return 'execute signageos power action';
  }

}
