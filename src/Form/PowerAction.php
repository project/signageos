<?php

namespace Drupal\signageos\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\digital_signage_framework\Form\ActionBase;
use Drupal\signageos\Plugin\Action\PowerAction as PowerActionAction;
use Drupal\signageos\Plugin\DigitalSignagePlatform\SignageOs;

/**
 * Push power actions to devices.
 */
class PowerAction extends ActionBase {

  /**
   * {@inheritdoc}
   */
  protected function id(): string {
    return 'signageos_power_action';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Push power action to selected devices');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Push');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $form['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Power action'),
      '#options' => PowerActionAction::powerActions(),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    if ($form_state->getValue('confirm')) {
      foreach ($this->devices as $device) {
        $plugin = $device->getPlugin();
        if (!($plugin instanceof SignageOs)) {
          continue;
        }
        $plugin->sendPowerAction($device, $form_state->getValue('action'));
      }
    }
  }

}
